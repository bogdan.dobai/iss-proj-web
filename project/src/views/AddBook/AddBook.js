import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import axiosInstance from '../../services/Axios/AxiosInstance';
import {ADD_BOOK} from '../../services/APIConstants/ApiConstants'
import {addBookViewStyles} from "./AddBookStyle";
import {withStyles} from "@material-ui/core/styles";
import MuiAlert from '@material-ui/lab/Alert';
import SnackBar from '@material-ui/core/Snackbar'


let title = ""
let author = ""
let publication = ""
let year = ""


function Alert(props){
    return <MuiAlert elevation={6} variant="filled" {...props}/>
}

class AddBook extends React.Component {
    constructor(props){
        super(props)
        this.state={open:false}
}
    // handleForm = (event) => {
    //     event.preventDefault();
    //     console.log(date_of_birth)
    // }

    

    addBook = (event) => {
        event.preventDefault();
        return axiosInstance.post(ADD_BOOK,{
            "title":title,
            "author":author,
           "publication":publication,
            "year":year,
            "isBorrowed":false
        }).then(
          (res) => {console.log(res); 
            this.setState({open:true})
            })
          .catch(err => console.error(err))
      }


      handleTitle = (event) => {
                title = event.target.value;
            }
        
    handleAuthor = (event) => {
              author = event.target.value;
            }
        
    handlePublication = (event) => {
              publication = event.target.value;
            }
        
    handleYear = (event) => {
                year = event.target.value;
            }

    handleClose = (event) => {
        this.setState({open:false})
    }

    render() {
      const {classes} = this.props;
      return (
        <Container component="main" maxWidth="xs">
          <CssBaseline />
          <div className={classes.paper}>
            <form className={classes.form} noValidate>
            <Typography component="h1" variant="h5">
              Add Book
            </Typography> 
             <Grid container spacing={2}>
                <Grid item xs={12}>
                  <TextField
                    variant="outlined"
                    required
                    fullWidth
                    id="title"
                    label="Title"
                    name="title"
                    onChange = { this.handleTitle }
                    className={classes.textField}
                    style={{borderColor:'red'}}
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    variant="outlined"
                    required
                    fullWidth
                    id="author"
                    label="Author"
                    name="author"
                    autoComplete="author"
                    onChange = { this.handleAuthor }
                  />
                </Grid>
                <Grid item xs={12} sm={6}>
                  <TextField
                    autoComplete="publication"
                    name="publication"
                    variant="outlined"
                    required
                    fullWidth
                    id="publication"
                    label="Publication"
                    autoFocus
                    onChange = { this.handlePublication }
                  />
                </Grid>
                <Grid item xs={12} sm={6}>
                  <TextField
                    variant="outlined"
                    required
                    fullWidth
                    id="year"
                    label="Year"
                    name="year"
                    autoComplete="year"
                    onChange = { this.handleYear }
                  />
                </Grid>
                <Grid item xs={12}>
                </Grid>
              </Grid>
              <Button
                type="submit"
                fullWidth
                variant="contained"
                color="#5AB9EA"
                className={classes.submit}
                onClick = { this.addBook }
              >
                  Add Book
              </Button>
              <SnackBar open={this.state.open} autoHideDuration={3000} onClose={this.handleClose}>
                  <Alert onClose={this.handleClose} severity="success">
                      Book was added successfully
                  </Alert>
              </SnackBar>
            </form>
          </div>
        </Container>
      );
    }
}





export default withStyles(addBookViewStyles)(AddBook)


// import React from 'react';
// import Button from '@material-ui/core/Button';
// import CssBaseline from '@material-ui/core/CssBaseline';
// import TextField from '@material-ui/core/TextField';
// import Grid from '@material-ui/core/Grid';
// import Container from '@material-ui/core/Container';
// import axiosInstance from '../../services/Axios/AxiosInstance';
// import {CREATE_USERS_REGISTER} from '../../services/APIConstants/ApiConstants'
// import {addBookViewStyles} from "./AddBookStyle";
// import {withStyles} from "@material-ui/core/styles";

// let title = ""
// let author = ""
// let publication = ""
// let year = ""



// class AddBook extends React.Component {

//     handleForm = (event) => {
//         event.preventDefault();
//         return axiosInstance.post(CREATE_USERS_REGISTER,{
//           title,
//           author,
//           publication,
//           year,
//         }).then(res => console.log(res))
//           .catch(err => console.error(err))
//       }

//     handleTitle = (event) => {
//         title = event.target.value;
//     }

//     handleAuthor = (event) => {
//       author = event.target.value;
//     }

//     handlePublication = (event) => {
//       publication = event.target.value;
//     }

//     handleYear = (event) => {
//         year = event.target.value;
//     }




//     render() {
//       const {classes} = this.props;
//       return (
//         <Container component="main" maxWidth="xs">
//           <CssBaseline />
//           <div className={classes.paper}>
//             <form className={classes.form} noValidate>
//               <Grid container spacing={2}>
//                 <Grid item xs={12}>
//                   <TextField
//                     variant="outlined"
//                     required
//                     fullWidth
//                     id="title"
//                     label="Title"
//                     name="title"
//                     autoComplete="title"
//                     onChange = { this.handleTitle }
//                   />
//                 </Grid>
//                 <Grid item xs={12}>
//                   <TextField
//                     variant="outlined"
//                     required
//                     fullWidth
//                     id="author"
//                     label="Author"
//                     name="author"
//                     type="author"
//                     autoComplete="current-author"
//                     onChange = { this.handleAuthor }
//                   />
//                 </Grid>
//                 <Grid item xs={12} sm={6}>
//                   <TextField
//                     autoComplete="publication"
//                     name="publication"
//                     variant="outlined"
//                     required
//                     fullWidth
//                     id="publication"
//                     label="Publication"
//                     autoFocus
//                     onChange = { this.handlePublication }
//                   />
//                 </Grid>
//                 <Grid item xs={12} sm={6}>
//                   <TextField
//                     variant="year"
//                     required
//                     fullWidth
//                     id="year"
//                     label="Last Name"
//                     name="year"
//                     autoComplete="year"
//                     onChange = { this.handleYear }
//                   />
//                 </Grid>
//                 <Grid item xs={12}>
//                 </Grid>
//               </Grid>
//               <Button
//                 type="submit"
//                 fullWidth
//                 variant="contained"
//                 color="primary"
//                 className={classes.submit}
//                 onClick = { this.handleForm }
//               >
//                 Add Book
//               </Button>
//             </form>
//           </div>
//         </Container>
//       );
//     }
// }





// export default withStyles(addBookViewStyles)(AddBook)
