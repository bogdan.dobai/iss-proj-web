import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';
import axiosInstance from '../../services/Axios/AxiosInstance';
import {GET_BOOKS, ADD_USER_BOOK} from '../../services/APIConstants/ApiConstants'
import {loginViewStyles} from "./HomeViewStyle";
import {withStyles} from "@material-ui/core/styles";
import { Table } from '@material-ui/core';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import MuiAlert from '@material-ui/lab/Alert';
import SnackBar from '@material-ui/core/Snackbar'

function Alert(props){
    return <MuiAlert elevation={6} variant="filled" {...props}/>
}

class HomeView extends React.Component {
    constructor(props){
        super(props);
        this.state={books:[],open:false,errorOpen:false}
    }
    componentDidMount(){
        axiosInstance.get(GET_BOOKS)
        .then(res => {
                console.log(this.state.books)
                this.setState({books: res.data})
            }
        ).catch(err => console.error(err))
    }

    addUserBook = (row) => {
        console.log('row',row)
        axiosInstance.post(ADD_USER_BOOK,{
            "userid":2,
            "bookid":row.id,
            "datetime":"12/12/2012",
            "penalty":0,
            "paidpenalty":0
        })
        .then(res => {
                this.setState({open: true})
                console.log(res)
            }
        ).catch(err => {
            this.setState({errorOpen: true})
            console.error(err)
        })
    }

    onClose = () => {
        this.setState({errorOpen:false,open:false})
    }

    render() {
        const {classes} = this.props;
        function createData(name, calories, fat, carbs, protein) {
            return { name, calories, fat, carbs, protein };
        }

        const rows = this.state.books

        return (
            <Container component="main" className={classes.container}>
                <CssBaseline/>
                <div className={classes.paper}>
                    <TableContainer component={Paper}>
                    <Table className={classes.table} aria-label="simple table">
                    <TableHead>
                    <TableRow>
                    <TableCell>Title</TableCell>
                    <TableCell align="right">Autor</TableCell>
                    <TableCell align="right">Publication</TableCell>
                    <TableCell align="right">Year</TableCell>
                    </TableRow>
                    </TableHead>
                    <TableBody>
                    {rows.length && rows.map((row) => (
                        <TableRow key={row.id}>
                            <TableCell component="th" scope="row">
                                {row.title}
                            </TableCell>
                            <TableCell align="right">{row.author}</TableCell>
                            <TableCell align="right">{row.publication}</TableCell>
                            <TableCell align="right">{row.year}</TableCell>
                    <TableCell align="right" onClick={() => this.addUserBook(row)}>
                    <Button
                type="submit"
                fullWidth
                variant="contained"
                color="#5AB9EA"
                className={classes.submit}
                onClick = { this.addBook }
              >
                        {row.isBorrowed?'+':'-'}
                </Button>
                        </TableCell>
                    </TableRow>
                    ))}
                    </TableBody>
                    </Table>
                    </TableContainer>
                    <SnackBar open={this.state.open} autoHideDuration={3000} onClose={this.onClose}>
                  <Alert onClose={this.onClose} severity="success">
                      The book has been successfully added to your collection
                  </Alert>
              </SnackBar>
              <SnackBar open={this.state.errorOpen} autoHideDuration={3000} onClose={this.onClose}>
                  <Alert onClose={this.onClose} severity="error">
                      An error occured. Please try again later
                  </Alert>
              </SnackBar>
                </div>
            </Container>
        )
    }
}


export default withStyles(loginViewStyles)(HomeView)
