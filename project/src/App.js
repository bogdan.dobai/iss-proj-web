import React from 'react';
import './App.css';
import {BrowserRouter as Router, Route, Switch, Redirect} from "react-router-dom";
import SideNav, { Toggle, Nav, NavItem, NavIcon, NavText } from '@trendmicro/react-sidenav';
import 'font-awesome/css/font-awesome.min.css';

import LoginView from "./views/Login/LoginView";
import RegisterView from "./views/Register/RegisterView";
import HomeView from "./views/Home/HomeView";
import AddBook from './views/AddBook/AddBook';
import DeleteBook from './views/DeleteBook/DeleteBook';

function App() {
    const defaultRoute =
        window.location.pathname === "/" ? <Redirect to="/home"/> : undefined;
    return (
        <Router>
            <div style={{display:'flex'}}>
                <div style={{display:'flex', boxShadow:10, position: 'sticky'}}>
                    <SideNav
                        onSelect={(selected) => {
                            window.location.href = `/${selected}`
                        }}
                        position="absolute"
                        style={{
                            float:'left',
                            width:'200px',
                            height: '100vh',
                            overflowX:'hidden',
                            paddingTop: 60,
                            justifyContent:'space-between',
                            backgroundColor:'white',
                            boxShadow: "0 10px 20px 0 rgba(25, 32, 56, 0.1)"
                        }}
                    >
                        <SideNav.Nav defaultSelected="home">
                            <NavItem style={{flexDirection:'row',flex:1, marginBottom:20}} eventKey="home">
                                <NavText style={{color: '#5AB9EA'}}>
                                    <i className="fa fa-fw fa-home" style={{ fontSize: '1.75em', marginRight:20 }} />
                                    Home
                                </NavText>
                            </NavItem>
                            <NavItem style={{flexDirection:'row',flex:1, marginBottom:20}} eventKey="delete">
                                <NavText style={{color: '#5AB9EA'}}>
                                    <i className="fa fa-fw fa-line-chart" style={{ fontSize: '1.75em', marginRight:20 }} />
                                    Librarian
                                </NavText>
                            </NavItem>
                            <NavItem style={{flexDirection:'row',flex:1, marginBottom:20}} eventKey="book">
                                <NavText style={{color: '#5AB9EA'}}>
                                    <i className="fa fa-fw fa-book" style={{ fontSize: '1.75em', marginRight:20 }} />
                                    Add Book
                                </NavText>
                            </NavItem>
                        </SideNav.Nav>
                    </SideNav>
                </div>
                <div style={{width:'100%', overflow:'auto'}}>
                    <Switch>
                        <Route path = {"/login"} component = {LoginView}/>
                        <Route path = {"/register"} component = {RegisterView}/>
                        <Route path = {"/home"} component={HomeView}/>
                        <Route path = {"/book"} component={AddBook}/>
                        <Route path = {"/delete"} component={DeleteBook}/>
                    </Switch>
                </div>
            </div>
            {defaultRoute}
        </Router>
    );
}

export default App;
