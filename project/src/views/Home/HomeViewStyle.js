export const loginViewStyles = (theme => ({
    container:{
      display:'flex',
        width:'100vw'
    },
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        height:'100%',
        width:'100%',
        justifyContent:'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
        height:40,
        width:40,
        fontSize:20,
        color:'white',
        textAlign:'center',
        backgroundColor:'#5AB9EA',
        justifyContent:'center',
        alignItems:'center'
    },
    table:{
        minWidth: 650,
        alignSelf:'center'
    },
    tableContainer:{
        justifyContent:'center',
        alignItems:'center',
        width:'100%',
    }
}));
